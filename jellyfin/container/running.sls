# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import jellyfin with context %}

{%- set mountpoint = jellyfin.docker.mountpoint %}

jellyfin-container-running-container-present:
  docker_image.present:
    - name: {{ jellyfin.container.image }}
    - tag: {{ jellyfin.container.tag }}
    - force: True

jellyfin-container-running-container-managed:
  docker_container.running:
    - name: {{ jellyfin.container.name }}
    - image: {{ jellyfin.container.image }}:{{ jellyfin.container.tag }}
    - restart: always
    - binds:
      - {{ mountpoint }}/docker/jellyfin/config:/config
      - {{ mountpoint }}/shares/media/tv:/data/tvshows
      - {{ mountpoint }}/shares/media/movies:/data/movies
      - {{ mountpoint }}/shares/audio/audiobooks:/data/audiobooks
      - {{ mountpoint }}/shares/audio/music:/data/music
    - port_bindings:
      - 8197:8096
    - environment:
      - PUID: 1000
      - PGID: 100
      - TZ: Europe/London
    - labels:
      - "traefik.enable=true"
      - "traefik.http.middlewares.jellyfin-redirect-websecure.redirectscheme.scheme=https"
      - "traefik.http.routers.jellyfin-web.middlewares=jellyfin-redirect-websecure"
      - "traefik.http.routers.jellyfin-web.rule=Host(`{{ jellyfin.container.url }}`)"
      - "traefik.http.routers.jellyfin-web.entrypoints=web"
      - "traefik.http.routers.jellyfin-websecure.rule=Host(`{{ jellyfin.container.url }}`)"
      - "traefik.http.routers.jellyfin-websecure.tls.certresolver=letsencrypt"
      - "traefik.http.routers.jellyfin-websecure.tls=true"
      - "traefik.http.routers.jellyfin-websecure.entrypoints=websecure"
