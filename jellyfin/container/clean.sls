# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import jellyfin with context %}

jellyfin-service-clean-service-dead:
  service.dead:
    - name: {{ jellyfin.service.name }}
    - enable: False
