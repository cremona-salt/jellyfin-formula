# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import jellyfin with context %}

{%- set mountpoint = jellyfin.docker.mountpoint %}

{%- for dir in [
  'docker/jellyfin/config',
  'shares/media/tv',
  'shares/media/movies',
  'shares/audio/audiobooks',
  'shares/audio/music'
  ]
%}
jellyfin-config-file-{{ dir|replace('/', '_') }}-directory-managed:
  file.directory:
    - name: {{ mountpoint }}/{{ dir }}
    - user: stooj
    - group: users
    - dir_mode: '0755'
    - file_mode: '0644'
    - makedirs: True
    - recurse:
      - mode
      - user
      - group
{% endfor %}

jellyfin-config-file-jellyfin-max-user-watches-configured:
  sysctl.present:
    - name: fs.inotify.max_user_watches
    - value: {{ jellyfin.max_user_watches }}
